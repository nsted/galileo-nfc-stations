byte TXBuffer[40];    // transmit buffer
byte RXBuffer[40];    // receive buffer
byte NFCReady = 0;  // used to track NFC state
byte Memory_Block = 0; // keep track of memory we write to
byte Data = 0; // keep track of memory we read from     

void BM019_begin() {

    delay(10);                      // send a wake up    
  
//    Serial.begin(57600);
//    Serial.println("what up");  
    Serial1.begin(57600);
    Serial1.write((byte)0x00);
    delay(10);                      // send a wake up  
    SetProtocol_Command();
}


/* SetProtocol_Command programs the CR95HF for
ISO/IEC 15693 operation.

This requires three steps.
1. send command
2. poll to see if CR95HF has data
3. read the response

If the correct response is received the serial monitor is used
to display successful programming. 
*/
int SetProtocol_Command(){
  int reply = 0; 
  
  Serial1.write((byte)0x02);  // protocol command
  Serial1.write((byte)0x02);  // length of data that follows is 0
  Serial1.write((byte)0x01);  // request Flags byte
  Serial1.write((byte)0x01);  // code for ISO/IEC 15693
  Serial1.write((byte)0x0D);  // mask length for inventory command

  delay(20);

  RXBuffer[0] = Serial1.read();  // response code
  RXBuffer[1] = Serial1.read();  // length of data
  
  for (byte i=0;i<RXBuffer[1];i++){     
    RXBuffer[i+2]=Serial1.read();  // data  
  }
 
  if ((RXBuffer[0] == 0) & (RXBuffer[1] == 0))  // is response code good?
    {
      reply = 1;
//    Serial.println("Protocol Set Command OK");
//    NFCReady = 1; // NFC is ready
    }
//  else
//    {
//    Serial.println("Protocol Set Command FAIL");
//    Serial.println(RXBuffer[0],HEX);
//    for (byte i=0;i<RXBuffer[1];i++){    
//      Serial.print(RXBuffer[i]);
//      Serial.print(',');
//    }
//    Serial.println();    
//    NFCReady = 0; // NFC not ready
//    }
  return reply;
}






/* Inventory_Command chekcs to see if an RF
tag is in range of the BM019.

This requires three steps.
1. send command
2. poll to see if CR95HF has data
3. read the response

If the correct response is received the serial monitor is used
to display the the RF tag's universal ID.  
*/
int Inventory_Command(byte uid[]){
  int reply = 0;
   
  Serial1.write((byte)0x04);  // Send Receive CR95HF command
  Serial1.write((byte)0x03);  // length of data that follows is 0
  Serial1.write((byte)0x26);  // request Flags byte
  Serial1.write((byte)0x01);  // Inventory Command for ISO/IEC 15693
  Serial1.write((byte)0x00);  // mask length for inventory command

  delay(20);             // min 20ms

//  if(Serial1.available()){
  RXBuffer[0] = Serial1.read();  // response code
  RXBuffer[1] = Serial1.read();  // length of data
  
  for (byte i=0;i<RXBuffer[1];i++){     
    RXBuffer[i+2]=Serial1.read();  // data  
  }

  if (RXBuffer[0] == 0x80)  // is response code good?
    {
      int j = 0;
      for(int i=11;i>=4;i--)
      { 
        uid[j] = RXBuffer[i];
        j++;
      }     
      reply = 1; 
      
//    Serial.println("Inventory Command OK");
//    NFCReady = 2;
    }
//  else
//    {
//    Serial.println("Inventory Command FAIL");
//    Serial.println(RXBuffer[0],HEX);
//    Serial.println(RXBuffer[1],HEX);    
//    for (byte i=0;i<RXBuffer[1];i++){    
//      Serial.print(RXBuffer[i],HEX);
//      Serial.print(',');
//    }
//    Serial.println();
////    NFCReady = 0;
//    }
  return reply;
 }
 
 
 
 
 
 
 
 
/* Write Memory writes data to a block of memory.  
This code assumes the RF tag has less than 256 blocks
of memory and that the block size is 4 bytes.  The block 
written to increments with each pass and if it exceeds
20 it starts over. Data is also changed with each write.

This requires three steps.
1. send command
2. poll to see if CR95HF has data
3. read the response
*/

int Write_Memory(int page, uint8_t pData[])
 {
  int reply = 0;

  Serial1.write((byte)0x04);  // Send Receive CR95HF command
  Serial1.write((byte)0x07);  // length of data that follows 
//  Serial1.write((byte)0x08);  //Use this length if more than 256 mem blocks 
  Serial1.write((byte)0x02);  // request Flags byte
//  Serial1.write((byte)0x0A);  // request Flags byte if more than 256 memory blocks
  Serial1.write((byte)0x21);  // Write Single Block command for ISO/IEC 15693
  Serial1.write((byte)page);  // Memory block address
//  Serial1.write((byte)0x00);  // MSB of mem. address greater than 256 blocks
  Serial1.write(pData[0]);  // first byte block of memory block
  Serial1.write(pData[1]);  // second byte block of memory block
  Serial1.write(pData[2]);  // third byte block of memory block
  Serial1.write(pData[3]);  // third byte block of memory block


  delay(20);             // min 20ms
//  if(Serial1.available()){
  RXBuffer[0] = Serial1.read();  // response code
  RXBuffer[1] = Serial1.read();  // length of data
  
  for (byte i=0;i<RXBuffer[1];i++){     
    RXBuffer[i+2]=Serial1.read();  // data  
  }

  if (RXBuffer[0] == 0x80 || RXBuffer[0] == 0x88)  // is response code good?
    {
//    Serial.print("Write Memory Block Command OK - Block ");
//    Serial.print(Memory_Block);
//    Serial.print(":  ");
//    Serial.print(Data);
//    Serial.print(" ");
//    Serial.print(Data+1);
//    Serial.print(" ");
//    Serial.print(Data+2);
//    Serial.print(" ");
//    Serial.print(Data+3);
//    Serial.println(" ");
//    NFCReady = 2;
    }
//  else
//    {
//    Serial.println("Write Memory Block Command FAIL");
//    Serial.println(RXBuffer[0],HEX);
//    Serial.println(RXBuffer[1],HEX);    
//    for (byte i=0;i<RXBuffer[1];i++){    
//      Serial.print(RXBuffer[i],HEX);
//      Serial.print(',');
//    }
//    Serial.println();    
//    NFCReady = 1;
//    }
  return reply;
 }







/* Read Memory reads data from a block of memory.  
This code assumes the RF tag has less than 256 blocks
of memory and that the block size is 4 bytes.  Data
read from is displayed via the serial monitor.

This requires three steps.
1. send command
2. poll to see if CR95HF has data
3. read the response
*/
int Read_Memory(int page, byte pData[])
 {
  int reply = 0;
   
  Serial1.write((byte)0x04);  // Send Receive CR95HF command
  Serial1.write((byte)0x03);  // length of data that follows
 // Serial1.write((byte)0x04);  // length of data if mem > 256
  Serial1.write((byte)0x02);  // request Flags byte
 // Serial1.write((byte)0x0A);  // request Flags byte if mem > 256
  Serial1.write((byte)0x20);  // Read Single Block command for ISO/IEC 15693
  Serial1.write((byte)page);  // memory block address
//  Serial1.write((byte)0x00);  // MSB of memory block address if mem > 256

  delay(20);             // min 20ms
  
  RXBuffer[0] = Serial1.read();  // response code
  RXBuffer[1] = Serial1.read();  // length of data
  
  for (byte i=0;i<RXBuffer[1];i++){     
    RXBuffer[i+2]=Serial1.read();  // data  
  }
 
  if (RXBuffer[0] == 0x80) // is response code good?
  {
    pData[0] = RXBuffer[3];
    pData[1] = RXBuffer[4];
    pData[2] = RXBuffer[5];
    pData[3] = RXBuffer[6];      
      
//    Serial.print("Read Memory Block Command OK - Block ");
//    Serial.print(Memory_Block);
//    Serial.print(":  ");
//    Serial.print(RXBuffer[3]);
//    Serial.print(" ");
//    Serial.print(RXBuffer[4]);
//    Serial.print(" ");
//    Serial.print(RXBuffer[5]);
//    Serial.print(" ");
//    Serial.print(RXBuffer[6]);
//    Serial.print(" -  ");
//    Serial.print((char)RXBuffer[3]);
//    Serial.print(" ");
//    Serial.print((char)RXBuffer[4]);
//    Serial.print(" ");
//    Serial.print((char)RXBuffer[5]);
//    Serial.print(" ");
//    Serial.print((char)RXBuffer[6]);
//    Serial.println(" ");    
//    NFCReady = 2;
  }
  else
    {
//    Serial.print("Read Memory Block Command FAIL ");
//    Serial.println(RXBuffer[0],HEX);
//    NFCReady = 1;
    }
  return reply;
 }
 
 
 
 
 int IDN_Command(){
  int reply = 0;
  
  Serial1.write((byte)0x01);
  Serial1.write((byte)0x00);   
   
  delay(50);             // min 20ms
  
//  while(!Serial1.available()){
//    Serial.println("wait");
//    delay(100);
//  }

  RXBuffer[0] = Serial1.read();  // response code
  RXBuffer[1] = Serial1.read();  // length of data
  
//  Serial.print(RXBuffer[0],HEX);
//  Serial.print(',');
//  Serial.println(RXBuffer[1],HEX);
  
  for (byte i=0;i<RXBuffer[1];i++){     
    RXBuffer[i+2]=Serial1.read();  // data  
  }
  
  if ((RXBuffer[0] == 0) & (RXBuffer[1] == 15))
  {
    reply = 1;  
  } 
  return reply;
} 
