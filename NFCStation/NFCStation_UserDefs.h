#ifndef NFCStation_UserDefs_h         // don't touch
#define NFCStation_UserDefs_h         // don't touch


///////////////////////////////////////////////////////////////////////////
/*

Customize the defines in this file for your needs

*/
///////////////////////////////////////////////////////////////////////////



// You probably need to change the following defs only once per event
#define COIN_BOOTHS (22)               // Number of regular coin Booths
#define REGISTRATION_BOOTHS (4)       // Number of registrations Booths
#define PRIZE_BOOTHS (1)               // Number of registrations Booths

#define COIN (1)                       // How many coins is this booth worth.
#define EVENT_ID (99)                  // A unique ID for the event - keep the same for all units at an event
#define EVENT_TAG_TYPE (ISO15693)      // select which NFC tag type is used for this event

#endif
