#ifndef NFCStation_h
#define NFCStation_h

#include "NFCStation_UserDefs.h"


#define MAX_COINS (COIN_BOOTHS + REGISTRATION_BOOTHS + PRIZE_BOOTHS)

#define COIN_TYPE (1)
#define REGISTRATION_TYPE (2)          // Registration Booth
#define PRIZE_TYPE (3)                 // Prize/Games Booth

#define NTAG216 (1)                // a unique id for the different types of NFC tags
#define NTAG213 (2)
#define ULTRALIGHT (3)
#define MIFARE_CLASSIC (4)
#define ISO15693 (5)

//Don't Mess with these Below
#define FIRST_NAME_LENGTH (20)    // Maximum Characters in a name
//#define LAST_NAME_LENGTH (20)     // Maximum Characters in a name
//#define EMAIL_LENGTH (48)         // Maximum Characters in a name
#define USER_ID_LENGTH (4)        // Bytes in the maximum possible number of users (4 = uint32_t)
#define COMMAS_NUM (1)            // How many commas will separate the chunks of data in the received data
//#define UART_INPUT_LENGTH (FIRST_NAME_LENGTH + LAST_NAME_LENGTH + EMAIL_LENGTH + COMMAS_NUM)      // no user ID in this install
#define UART_INPUT_LENGTH (FIRST_NAME_LENGTH + USER_ID_LENGTH + COMMAS_NUM)      
#define P_LENGTH (4)              // Bytes in a Page
#define B_LENGTH (4)              // Bytes in a BLOCK

#define CHECKSUM_P  (3)             // The checksum
#define USER_ID_P (4)             // Page Address of User ID
#define EVENT_ID_P (5)            // Page Address of Event ID
#define FIRST_NAME_P (6)          // Page Address of the User's first name
#define COINS_P (FIRST_NAME_P + (FIRST_NAME_LENGTH / P_LENGTH))  // Page Address of the User's last name
//#define LAST_NAME_P (FIRST_NAME_P + (FIRST_NAME_LENGTH / P_LENGTH))  // Page Address of the User's last name
//#define EMAIL_P (LAST_NAME_P + (LAST_NAME_LENGTH / P_LENGTH)) // Page Address of the User's email
//#define COINS_P (EMAIL_P + (EMAIL_LENGTH / P_LENGTH))     // Page Address of booth coins
#define COINS_LOCAL_P (stationId / P_LENGTH)    //Page address of this booth's local coins page


//Adafruit Defines
#define IRQ   (2)
#define RESET (3)                // Not connected by default on the NFC Shield

//Adafruit_NFCShield_I2C nfc(IRQ, RESET);
LiquidCrystal lcd(0);


//Variables that are holdover from the Adafruit examples
uint8_t uidLength;                        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
uint8_t uid[] = { 
    0, 0, 0, 0, 0, 0, 0                                               };  // Buffer to store the returned UID

int led = 8;
int wristbandNFCTagType = NTAG213;        // Which kind of NFC tag is it
int boothType;

// For the user info that is sent to the station over UART, stored & retrieved from the Wristband
struct userInfo{
  long eventID;
  String firstName;
  String userID;
//  String lastName;
//  String email;
  uint8_t coins[MAX_COINS] = {0};
  long checksum;    
};

userInfo readUserInfoFromWB();
userInfo parseUserInput(String userInput);
void registerUser(userInfo user);

// network related
int stationId;
String stationIp;
String serverIp;
String gatewayIp;

String inetDevice;

#endif
