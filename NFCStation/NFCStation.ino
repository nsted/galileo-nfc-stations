///////////////////////////////////////////////////////////////////////////
/*

  This program runs NFC scanning stations meant to voluntarily track User's Booth Visits on a conference/trade show floor.
  Three types of stations are supported: 
    1. Registration booths that store user data sent from a terminal on an NFC tag (a Wristband),
    2. Floor booths that log user visits on the NFC Tag,
    3. Prize booths that read User Data off NFC Tag, and send to a terminal.

 Prototype Demo Here: http://youtu.be/XzZos75glTE

  Hardware:
      Intel Galileo - http://arduino.cc/en/ArduinoCertified/IntelGalileo 
      Adafruit NFC shield - https://learn.adafruit.com/adafruit-pn532-rfid-nfc
      Adafruit LCD I2C Backpack - https://learn.adafruit.com/i2c-spi-lcd-backpack
      16x2 LCD - https://www.adafruit.com/product/181
      
    NFC tags must be Mifare Ultralight variety.  
  
  Required Libraries (place in Arduino libraries folder).
    Modified Adafruit_NFCShield Library - https://bitbucket.org/nsted/adafruit_nfcshield_i2c
    Adafruit LiquidCrystal Library - https://github.com/adafruit/LiquidCrystal

  TroubleShooting:
    If any erroneous info appears on LCD, hit reset button to see if that clears problems
  
*/
///////////////////////////////////////////////////////////////////////////



#include <Wire.h>
#include "LiquidCrystal.h"
#include <SPI.h>
#include <SD.h>
#include "NFCStation.h"

void setup(void) {
  Serial.begin(57600);
  lcd.begin(16, 2);
  delay(100);

  stationId = getId("/home/root/nfc-params/stationId.txt");   
  
  if( (stationId >= 0) && (stationId < REGISTRATION_BOOTHS) ){
    boothType = REGISTRATION_TYPE;
  } else if( (stationId >= REGISTRATION_BOOTHS) && (stationId < REGISTRATION_BOOTHS + PRIZE_BOOTHS) ){
    boothType = PRIZE_TYPE;
  } else {
    boothType = COIN_TYPE;  
  }   
  
  if(boothType == REGISTRATION_TYPE){
    system("cd /media/sdcard/");
  } 
  else {
    system("cd /media/mmcblk0p1/");
  }  
  
  lcd.clear();  
  lcd.setCursor(0, 0);   
  lcd.print("Booting...");    
 
  BM019_begin();

  pinMode(led, OUTPUT);                 // prep status led that blinks when Wristband Detected
  ledFlash(6);    
  
  int versiondata = IDN_Command();
  while (versiondata == 0) {
    delay(100);   
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("NFC");
    lcd.setCursor(0, 1);     
    lcd.print("Restart"); 
    versiondata = IDN_Command();
  }  

  if(boothType != REGISTRATION_TYPE){
    networkSetup();
    printNetworkInfo();
  }

// FOR TESTING ONLY
//  String tester = "ping 172.16.19.246 -c 3 > /dev/ttyGS0 2>/dev/ttyGS0\n";
//  systemOut(tester);
//  tester = "ifconfig > /dev/ttyGS0\n";
//  systemOut(tester);
//  userInfo user;
//  user.firstName = "Nick";
//  user.userID = "2";
//  user.checksum = checkSum(user);
//  networkAnounce(user);
  
}


///////////////////////////////////////////////////////////////////////////
/*

Main Loop & High Level routines

*/
///////////////////////////////////////////////////////////////////////////


void loop(void) { 
  switch(boothType){
    case REGISTRATION_TYPE:
      registrationBoothHandler();
      break;
    case PRIZE_TYPE:
      prizeBoothHandler();
      break;
    default:
      coinBoothHandler();
      break;
  }

//  timeStamp();
}

void timeStamp(){
  static long lastSeconds;
  long seconds = millis()/1000;
  if(seconds != lastSeconds) {
    Serial.println(seconds);
    lastSeconds = seconds;
  }  
}

// If a registration booth then greet, and check for User Input over UART. If not available try to award coins.
void registrationBoothHandler(){                               
  static int lcd_state = 0;                         // keep track of what's on lcd
  
  if(lcd_state != 1){                                  
    lcd_state = 1;      
    registrationBoothGreeting();                    // say hi!
  }

  if(Serial.available()){                           // if user info sent over serial port, read it and store it
     registration();    
     lcd_state = 2;
  } 
//  else {
//    uint8_t success = coinCollection();             // otherwise try to award a coin
//    lcd_state += success;
//  }
}  

// If a prize booth then greet and see if prize can be awarded
void prizeBoothHandler(){  
  static int lcd_state = 0;                         // keep track of what's on lcd
  
  if(lcd_state != 1){
    lcd_state = 1;      
    prizeBoothGreeting();                          // say hi!
  }  
  
  uint8_t success = prizeAssignment();             // try to award a prize
  if(success){
    lcd_state = 2;
  }  
}      

void coinBoothHandler(){  
  static int lcd_state = 0;                         // keep track of what's on lcd
  
  if(lcd_state != 1){
    lcd_state = 1;      
    coinBoothGreeting();                            // say hi!
  }
  
  uint8_t success = coinCollection();              // try to award a coin
  if(success){
    lcd_state = 2;
  }
}

///////////////////////////////////////////////////////////////////////////
/*

 Main Functions of different station types 

*/
///////////////////////////////////////////////////////////////////////////

//  Try to register User
uint8_t registration(){
    
  userInfo user = userInputReadFromUART();            // Read user info from UART
  user.eventID = (EVENT_ID);                          // They registered so give them a valid event id
 
  greetings(user.firstName);                          // Greet user by name
  lcd.setCursor(0, 1);
  lcd.print("Scan Wristband");                        // Tell them what to do
  
  long timeStamp = millis();
  while(millis() - timeStamp < 10000){                // wait up to this many milliseconds for wristband to register
    uint8_t wristbandId[8] = {0};
    uint8_t wristbandPresent = checkForWB(EVENT_TAG_TYPE, wristbandId);
    if(wristbandPresent){                             // if there's a valid tag
//      awardCoin(user.coins);                          // give user a coin
//      networkAnounce(user);      

      user.checksum = checkSum(user);
      
      bool isSame = false;
      int attemptsRemaining = 10;
      userInfo userConfirm;
      
      while((!isSame) && attemptsRemaining){
        attemptsRemaining--;
        if(!attemptsRemaining){
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("hmm, didn't work");
          lcd.setCursor(0,1);
          lcd.print("try again");
          delay(2500);
        }
        else {
          userInfoWriteToWB(user);                        // save user info on Wristband
          userConfirm = userInfoReadFromWB();   
          isSame = compareUsers(user, userConfirm);  // double check it
    //          if(isSame) {
    //            Serial.println("users match");
    //          } 
    //          else {
    //            Serial.println("users differ!!!");
    //          }
          delay(10);
        }
      
        if(isSame){
          ledFlash(4);                                    // indicate that user can move their hand/Wristband again
          String userInfoString = userInfoToString(userConfirm);
          
          Serial.println(userInfoString);                 // send user info out UART
          
          lcd.clear();                                    // confirm with lcd message
          greetings(user.firstName);                              // say what's up
          lcd.setCursor(0, 1);    
          lcd.print("you registered!");   
    
          // writeToSD(userInfoString);                      // save it on SD card
          delay(2500);
        }
      }  
   
      return 1;      
    }
  } 
  lcd.clear();                                        // confirm with lcd message
  lcd.setCursor(0, 0);     
  lcd.print("Timed Out!");   
  lcd.setCursor(0, 1);     
  lcd.print("Try Again");     
  delay(3000);
  
  return 0;
}


// Main function of the prize booths
uint8_t prizeAssignment(){
  uint8_t wristbandId[8] = {0};
  uint8_t wristbandPresent = checkForWB(EVENT_TAG_TYPE, wristbandId);
  delay(10);
  
  if(wristbandPresent){                                       // if found
    userInfo user;
    userInfo userConfirm;    
    bool isSame = false;
    int attemptsRemaining = 10;
    
    while((!isSame) && attemptsRemaining){
      attemptsRemaining--;

      user = userInfoReadFromWB();                     // read the user info from it
      isSame = (user.checksum == checkSum(user));
    
//      if(isSame){
//        Serial.println("checksums match ");
//      }
//      else {
//        Serial.println("checksums differ");
//        Serial.println(user.checksum);
//        Serial.println(checkSum(user));      
//      }
    }

    uint8_t validEvent = validateEvent(user.eventID);         

    if(isSame && validEvent){                                 // if event is valid, ie. NFC device is the intended Wristband
      uint8_t coinPresent = awardCoin(user.coins); 
      user.checksum = checkSum(user);
      int coinTally = tally(user.coins);
//      if(!coinPresent){
//        coinWriteToWB(user);                                  // give a coin if not already there
        networkAnounce(user);
        if(isSame){
          // try validating writes if confident that user data was read correctly.
        }
//      }
      ledFlash(4);                                            // cue user they can move their hand  
      
      String userInfoString = userInfoToString(user);           
//      Serial.print(arrayToCsvString(wristbandId, sizeof(wristbandId)) + ",");      
      Serial.println(userInfoString);                         // send the user info out UART (usually no connected device, so generally ignored)
      
      greetings(user.firstName);                              // say what's up
      lcd.setCursor(0, 1);                
      lcd.print("you got ");
      lcd.print(coinTally);
      lcd.print(" coins");

      // writeToSD(userInfoString);                      // save it on SD card
      
      delay(3000); 
    } else {
      ledFlash(4);       
      tagDissallow();                                        // uh oh, event was invalid...something's wrong with that scan
      delay(4000);
    } 
    return 1;    
  }  
  return 0;
}


// Main function of the coin booths
uint8_t coinCollection(){
  uint8_t wristbandId[8] = {0};
  uint8_t wristbandPresent = checkForWB(EVENT_TAG_TYPE, wristbandId);
  delay(10);

  if(wristbandPresent){                                       // if found
    userInfo user;
    userInfo userConfirm;    
    bool isSame = false;
    int attemptsRemaining = 10;
    
    while((!isSame) && attemptsRemaining){
      attemptsRemaining--;

      user = userInfoReadFromWB();                     // read the user info from it
      isSame = (user.checksum == checkSum(user));
    
//      if(isSame){
//        Serial.println("checksums match ");
//      }
//      else {
//        Serial.println("checksums differ");
//        Serial.println(user.checksum);
//        Serial.println(checkSum(user));      
//      }
    }

    uint8_t validEvent = validateEvent(user.eventID);         

    if(isSame && validEvent){                                 // if event is valid, ie. NFC device is the intended Wristband
      uint8_t coinPresent = awardCoin(user.coins); 
      user.checksum = checkSum(user);
      int coinTally = tally(user.coins);
      if(!coinPresent){
        coinWriteToWB(user);                                  // give a coin if not already there
        networkAnounce(user);
        if(isSame){
          // try validating writes if confident that user data was read correctly.
        }
      }
      ledFlash(4);                                            // cue user they can move their hand  
      
      String userInfoString = userInfoToString(user);           
//      Serial.print(arrayToCsvString(wristbandId, sizeof(wristbandId)) + ",");      
//      Serial.println(userInfoString);                         // send the user info out UART (usually no connected device, so generally ignored)

      if(coinPresent){                                        // tell the User what's up
        lcd.clear();
        lcd.setCursor(0, 0);
        greetings(user.firstName);    
        delay(2000);
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("You already got");
        lcd.setCursor(0, 1);
        lcd.print("this coin!");      
        delay(3000);
      } else {            
        greetings(user.firstName);          
        lcd.setCursor(0, 1);                
        lcd.print("you got a coin! Total Coins: ");
        lcd.print(coinTally);
       
        delay(2500);          
        for(int i = 0; i < 16; i++){
          lcd.scrollDisplayLeft();
          delay(50);
        }
        delay(2500);
      }  
      
      // writeToSD(userInfoString);                      // save it on SD card
                          
    } else {   
      ledFlash(4);       
      tagDissallow();                                        // uh oh, event was invalid...something's wrong with that scan
      delay(4000);
    } 
    return 1;    
  }  
  return 0;
}  

///////////////////////////////////////////////////////////////////////////
/*

NFC tag validating

*/
///////////////////////////////////////////////////////////////////////////

uint8_t checkForWB(int tagType, uint8_t wristbandId[]){                      //TODO: handle other types of tags
  uint8_t success = 0;
  
  switch(tagType){
    case NTAG213:
      success = checkForType2tag();
      break;
    case ULTRALIGHT:
      success = checkForType2tag();
      break;
    case NTAG216:
      success = checkForType2tag(); 
      break; 
    case ISO15693:
      success = checkForISO15693(wristbandId);
      break;
  }
  return success;
}

int checkForISO15693(uint8_t wristbandId[]){
//  uint8_t wristbandId[8] = {0}; 
  int success = 0; 
  
  Inventory_Command(wristbandId);
  
  for(int i=0; i<8; i++){
    success += wristbandId[i];
//    Serial.print(wristbandId[i],HEX);
//    Serial.print(',');
  }
  if (success > 1) success = 1;
  
//  Serial.println(success);
  return success;
}

uint8_t checkForType2tag(){
  uint8_t NFC_success; 
  uint8_t type2_success = 0; 
  
//  NFC_success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength);
  
  if(NFC_success){
    if(uidLength == 7){
      type2_success = 1;
    }
  }
  return type2_success;
}

//uint8_t checkForNFC(){
//  uint8_t success;
//  success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength);
//  return success;
//}


///////////////////////////////////////////////////////////////////////////
/*

 Read and parse the available user info
 from the String sent by the Registration Terminal.

*/
///////////////////////////////////////////////////////////////////////////


userInfo userInputReadFromUART(){
  char userInput[UART_INPUT_LENGTH] = {0};
  userInfo user;
  
  Serial.readBytesUntil('\r', userInput, UART_INPUT_LENGTH);
  Serial.read();
  
//  user.firstName = userInput;
  user.eventID = EVENT_ID;          

  String _userInput = userInput;      
    
  int split = _userInput.indexOf(',');  
  user.firstName = _userInput.substring(0,split);       // slice off the first field
  user.userID = _userInput.substring(split + 1);
//  user.

//  String remaining = _userInput.substring(split + 1);
//  split = remaining.indexOf(',');    
//  user.lastName = remaining.substring(0,split);         // slice off the second field
//  remaining = remaining.substring(split + 1);    
//  split = remaining.indexOf(',');      
//  user.email = remaining.substring(split + 1);           // slice off the third field 
//                                                               

  return user;
}

///////////////////////////////////////////////////////////////////////////
/*

 Log Data onto an SD card. 
 Used to backup User Data written to Wristband.

*/
///////////////////////////////////////////////////////////////////////////

void writeToSD(String dataString){
  pinMode(10, OUTPUT);        //set to output for SD card use...necessary for Galileo?

  if(!SD.exists("datalog.txt")){
//    Serial.println("datalog.txt does not exist, creating.");
    system("touch datalog.txt");
    delay(500);
  }

  File dataFile = SD.open("datalog.txt", FILE_WRITE);

  if (dataFile) {
    dataFile.println(dataString);
    dataFile.close();
  } 
}

///////////////////////////////////////////////////////////////////////////
/*

 Write to Wristband

*/
///////////////////////////////////////////////////////////////////////////

// Write User Info to Wristband
void userInfoWriteToWB(userInfo user){        // for safety, write from lowest to highest page address so valid data is not overwritten
  NFCWrite(user.userID, USER_ID_P);
  NFCWrite(user.firstName, FIRST_NAME_P);
//  NFCWrite(user.lastName, LAST_NAME_P);
//  NFCWrite(user.email, EMAIL_P);  
  NFCWrite(user.coins, MAX_COINS, COINS_P); 
  NFCWrite(EVENT_ID, EVENT_ID_P);
  NFCWrite(user.checksum, CHECKSUM_P);
}


void coinWriteToWB(userInfo user){
//        NFCWrite(user.coins, MAX_COINS, COINS_P);
  uint8_t localPage[P_LENGTH] = {0};  
  memcpy(&localPage[0], &user.coins[COINS_LOCAL_P * P_LENGTH], P_LENGTH);   
  NFCWrite(localPage, P_LENGTH, COINS_P + COINS_LOCAL_P);  
  NFCWrite(user.checksum, CHECKSUM_P);  
}

void NFCWrite(String _string, int _pageAddress){
  int _length = _string.length() + 1;                   // add one to length to capture null character
  uint8_t _array[_length];                              // !! last char is for null termination, eg. 4 bytes = 15 user chars + null
  _string.getBytes(_array, _length);                    
  
  NFCWrite(_array, _length, _pageAddress);
}

void NFCWrite(uint8_t _array[], int _length, int _pageAddress){
  uint8_t pI;                                                  // page index  

  for(pI = 0; pI < _length - 4; pI += 4){                      // send all the pages of data available
    uint8_t pData[4] = {0};
    memcpy(&pData[0], &_array[pI], 4);
//    nfc.type2tag_WritePage (_pageAddress, pData);    
    Write_Memory(_pageAddress, pData);
    _pageAddress++;    
  }

  uint16_t remainder = _length % 4;
  if(remainder > 0){                                       // if there's an incomplete page worth of data, pack them in a page and send them as well
    uint8_t pData[4] = {0};
    memcpy(&pData[0], &_array[pI], remainder);
    Write_Memory(_pageAddress, pData);    
//    nfc.type2tag_WritePage (_pageAddress, pData);  
  } else {                                                //  if there's a whole page worth of data at the end, send it as normal
    uint8_t pData[4] = {0};
    memcpy(&pData[0], &_array[pI], 4);
//    nfc.type2tag_WritePage (_pageAddress, pData);    
    Write_Memory(_pageAddress, pData);
  }
}


void NFCWrite(uint32_t _long, int _pageAddress){
  uint8_t pData[4] = {_long & 0xFF, (_long >> 8) & 0xFF, (_long >> 16) & 0xFF,  (_long >> 24) & 0xFF};
//  nfc.type2tag_WritePage(_pageAddress, pData);    
  Write_Memory(_pageAddress, pData);
}

///////////////////////////////////////////////////////////////////////////
/*

 Read from Wristband

*/
///////////////////////////////////////////////////////////////////////////

// Read User Info from Wristband
userInfo userInfoReadFromWB(){
  userInfo user;  

  user.checksum = NFCReadLong(CHECKSUM_P);
  user.eventID = NFCReadLong(EVENT_ID_P);
  user.userID = NFCReadString(USER_ID_P, USER_ID_LENGTH);
  user.firstName = NFCReadString(FIRST_NAME_P, FIRST_NAME_LENGTH);
//  user.lastName = NFCRead(LAST_NAME_P, LAST_NAME_LENGTH);
//  user.email = NFCRead(EMAIL_P, EMAIL_LENGTH);  
  NFCReadArray(user.coins, COINS_P, MAX_COINS);
    
  return user;
}

String NFCReadString(int _page, int _length){
  uint8_t _raw[B_LENGTH];  
  String _string;  
  
  for(int i = 0; i < _length; i += B_LENGTH){
    int chunk = _length - i;
    int len = min(chunk, B_LENGTH);
    Read_Memory(_page + (i / P_LENGTH), _raw);
    for(int j = 0; j < B_LENGTH; j++){ 
      if((_raw[j] == '\0')){
        return _string;
      }
      _string += (char)_raw[j];           
    }
  }
}

void NFCReadArray(uint8_t _array[], int _page, int _length){
  uint8_t _raw[B_LENGTH];
  
  for(int i = 0; i < _length; i += B_LENGTH){
    int chunk = _length - i;
    int len = min(chunk, B_LENGTH);
    Read_Memory(_page + (i / P_LENGTH), _raw);
    memcpy(&_array[i], &_raw[0], len);
  }  
}

long NFCReadLong(int _page){
  long _long = 0;
  
  uint8_t _raw[P_LENGTH];
  Read_Memory(_page, _raw);  
  for(int i = 0; i < P_LENGTH; i++){
    _long += _raw[i] << (i * 8);
  }
  return _long;
}

///////////////////////////////////////////////////////////////////////////
/*

 User Info Helper Functions

*/
///////////////////////////////////////////////////////////////////////////

int tally(uint8_t coins[]){
  int cTally = 0;
   
  for(int i = 0; i < MAX_COINS; i++){
    cTally += coins[i];
  }  
  return cTally;
}


uint8_t validateEvent(uint8_t eventID){
  if(eventID == EVENT_ID){
    return 1;
  } else {
    return 0;
  }  
}

uint8_t awardCoin(uint8_t coins[]){                          // get the arr
//  for(int i = 0; i < MAX_COINS; i++){
//    Serial.print(i);
//    Serial.print(',');
//    Serial.print(i - (REGISTRATION_BOOTHS + PRIZE_BOOTHS));
//    Serial.print(',');
//    Serial.println(coins[i - (REGISTRATION_BOOTHS + PRIZE_BOOTHS)]);
//  }
  if(coins[stationId] == COIN){
    return 1;
  } else {
    coins[stationId] = COIN;   
    return 0;
  }
}

String userInfoToString(userInfo user){
  String userInfoString;

  userInfoString += user.userID;
  userInfoString += ',';  
  userInfoString += user.firstName;
//  userInfoString += ',';
//  userInfoString += user.lastName;
//  userInfoString += ',';
//  userInfoString += user.email;
  
  for(int i = 0; i<MAX_COINS; i++){
    userInfoString += ',';
    userInfoString += user.coins[i];
  }
  
  userInfoString += ';';
  userInfoString += user.checksum;
  
  return userInfoString;
}

///////////////////////////////////////////////////////////////////////////
/*

General Purpose Helper Functions

*/
///////////////////////////////////////////////////////////////////////////

void ledFlash(int flashes){
  for(int i=0; i<flashes; i++){            // Blink LED because bracelet is detected
    digitalWrite(led, LOW);
    delay(50);
    digitalWrite(led, HIGH);
    delay(50);
  } 
}  

long min(long x, long y){
  if(x <= y){
    return x;
  } else {
    return y;
  }  
}

///////////////////////////////////////////////////////////////////////////
/*

 LCD messages

*/
///////////////////////////////////////////////////////////////////////////

void prizeBoothGreeting(){
  //Serial.println("I am the prize booth.");
  lcd.clear();
  lcd.setCursor(0, 0);  
  lcd.print("Scan Wristband");
  lcd.setCursor(0, 1);
  lcd.print("To Play Game");   
}  

void registrationBoothGreeting(){
  lcd.clear();
  lcd.setCursor(0, 0);  
  lcd.print("Register Here");
//  lcd.print("Register or");  
//  lcd.setCursor(0, 1);
//  lcd.print("Collect Coin"); 
}

void coinBoothGreeting(){
  lcd.clear();
  lcd.setCursor(0, 0);  
  lcd.print("Scan Wristband");
  lcd.setCursor(0, 1);
  lcd.print("to Collect Coin");   
}

void greetings(String _name){  
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Hi ");
  lcd.print(_name);
}

void tagDissallow(){
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Oops, try again");
  lcd.setCursor(0,1);
  lcd.print("or ask for help");
}

///////////////////////////////////////////////////////////////////////////
/*

 Linux & Network Related

*/
///////////////////////////////////////////////////////////////////////////

void systemOut(String input){
  int len = input.length();
  char inputChar[len];
  input.toCharArray(inputChar, len);
  system(inputChar);  
}

int getId(char file[]){
  char output[16];
  FILE *fp;
  fp = fopen(file, "r");
  fgets(output, 16, fp);
  fclose(fp); 
  return atoi(output);
}


String getIp(char file[]){
  char output[100];
  char * cleaned;
  FILE *fp;
  fp = fopen(file, "r");
  fgets(output, 100, fp);
  cleaned = strtok(output,"\n");
  strcpy(output, cleaned);
  fclose(fp); 
  return output;
}

void networkSetup(){

  system("touch /home/root/nfc-params/device.txt\n");
  system("ifconfig | grep eth0 > /home/root/nfc-params/device.txt\n");
  String device = getIp("/home/root/nfc-params/device.txt");
  if(device.length() > 10) {
    inetDevice = "eth0";
  }
  else {
    inetDevice = "enp0s20f6";
  }
  
  stationIp = getIp("/home/root/nfc-params/stationIp.txt");
  String setStaticIp;
  if(inetDevice.equals("eth0")){
    setStaticIp = "ifconfig eth0 " + stationIp +" netmask 255.255.255.0 up\n";
  }
  else {
    setStaticIp = "ifconfig enp0s20f6 " + stationIp +" netmask 255.255.255.0 up\n";
  }

  system("ifup enp0s20f6 > /dev/ttyGS0\n");
  system("ifup eth0 > /dev/ttyGS0\n");
//  systemOut(setStaticIp);  

  //authKey = getIp("/home/root/nfc-params/authkey.txt");
  serverIp = getIp("/home/root/nfc-params/serverIp.txt");  
  gatewayIp = getIp("/home/root/nfc-params/gateway.txt");
  String dns1Ip = getIp("/home/root/nfc-params/dns_1.txt");
  String dns2Ip = getIp("/home/root/nfc-params/dns_2.txt");  
  
//  Serial.println(serverIp);
//  Serial.println(stationIp);
//  Serial.println(gatewayIp);
//  Serial.println(dns1Ip);
//  Serial.println(dns2Ip);

//  String gw_setup = "route add default gw " + gatewayIp + "\n";
//  systemOut(gw_setup);              // sets the gateway address
//  
//  String dns1_setup = "echo nameserver " + dns1Ip +" > /etc/resolv.conf\n";
//  systemOut(dns1_setup);      //sets DNS (if needed)
//  
//  String dns2_setup = "echo nameserver " + dns2Ip +" >> /etc/resolv.conf\n";
//  systemOut(dns2_setup);
} 

void printNetworkInfo(){
  
  // create a blank file
  system("touch /home/root/nfc-params/inet_info.txt\n");
  // grep inet data from ifconfig
  if(inetDevice.equals("eth0")){
    system("/sbin/ifconfig eth0 | grep 'inet ' > /home/root/nfc-params/inet_info.txt\n");
  }
  else {
    system("/sbin/ifconfig enp0s20f6 | grep 'inet ' > /home/root/nfc-params/inet_info.txt\n");
  }    
  // remove the leading spaces
  system("sed \"s/^[ \t]*//\" -i /home/root/nfc-params/inet_info.txt\n");
  String inet = getIp("/home/root/nfc-params/inet_info.txt");
  int splitIndex = inet.indexOf('B');
  int splitIndex2 = inet.indexOf('M');  
  String bcast = inet.substring(splitIndex, splitIndex2);
  String mask = inet.substring(splitIndex2);
  String addr = inet.substring(0, splitIndex);
  
//  lcd.clear();
//  lcd.setCursor(0, 0);
//  lcd.print("ip: " + stationIp);
//  lcd.setCursor(0, 1);
//  lcd.print("gw: " + gatewayIp);  
//  delay(2500);
  lcd.clear();  
  lcd.setCursor(0, 0);
  lcd.print("sv: " + serverIp);  
  lcd.setCursor(0, 1);
  lcd.print(addr);
  int len = addr.length();    
//  Serial.println(len);
  
  delay(1000);
  for(int i = 0; i < len - 16; i++){
    lcd.scrollDisplayLeft();
    delay(300);
  }
  delay(2500);  
  
//  lcd.clear();  
//  lcd.setCursor(0, 0);
//  lcd.print(bcast);  
//  lcd.setCursor(0, 1);
//  lcd.print(mask);
//  len = bcast.length();    
////  Serial.println(len);
//  
//  delay(1500);
//  for(int i = 0; i < len - 16; i++){
//    lcd.scrollDisplayLeft();
//    delay(300);
//  }
//  delay(2500);    
}  

void networkAnounce(userInfo user){
  //* debug version - prints to Serial Monitor
//  String post = "curl -X POST -d user=" + user.userID + " -d station=1 " + serverIp + "/token > /dev/ttyGS0\n"; 
//  Serial.println();
//  Serial.println(post);

  if(boothType == PRIZE_TYPE){
//    String post = "curl -X POST -d player=1 -d station=1 172.16.19.246/api/core/gameplays/ > /dev/ttyGS0\n";
    String post = "curl -X POST -d player=" + user.userID + " -d station=1 " + serverIp + "/gameplays/\n";
    systemOut(post);
  }
  else{
    String post = "curl -X POST -d player=" + user.userID + " -d achievement=" + stationId + " " + serverIp + "/player-achievements/\n";    
//    String post = "curl -X POST -d player=1 -d achievement=10 172.16.19.246/api/core/player-achievements/ > /dev/ttyGS0\n";    
    systemOut(post);
  } 
}


int checkSum(userInfo user) {
  int checksum = 0;
  
//  checksum += user.eventID;
  for(int i = 0; i < user.firstName.length(); i++){
    checksum += user.firstName.charAt(i);
  }
  checksum += user.userID.toInt();
  for( int i = 0; i < MAX_COINS; i++ ){
    checksum += user.coins[i];
  }
  
  checksum = (~checksum) & 0xFFFF;    //invert and isolate 2 bytes
  
  return checksum;
}


bool compareUsers(userInfo user1, userInfo user2){
  bool isSame = true;
  
//  String u1 = userInfoToString(user1);
//  String u2 = userInfoToString(user2);
//  Serial.println(u1 + " " + u2);  
  
  isSame = user1.firstName.equals(user2.firstName);
  if(!isSame) {
    return false;
  }
  isSame = user1.userID.equals(user2.userID);
  if(!isSame) { 
    return false;
  }
  isSame = (user1.eventID == user2.eventID);
  if(!isSame) {
    return false;
  }
  
  int isNotSame = memcmp(user1.coins, user2.coins, MAX_COINS);
  if(isNotSame) {
    return false;
  }  

  isSame = (user1.checksum == user2.checksum);
  if(!isSame) {
    return false;
  }  
  
  return true;
}

