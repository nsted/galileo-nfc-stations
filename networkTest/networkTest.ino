#define IP_PREFIX ("192.168.0.")
#define HTTP_PORT (":8080")

int serverId;
int stationId;

String serverIp;
String stationIp;

void setup() {  
  serverId = getId("serverId_config.txt");
//  serverId = "http://intelmwc2015local.herokuapp.com";
  stationId = getId("stationId_config.txt");

//  serverIp = IP_PREFIX + String(serverId) + HTTP_PORT;
  serverIp = "http://intelmwc2015local.herokuapp.com";  
  stationIp = IP_PREFIX + String(stationId);


  Serial.begin(9600);

// use a static IP Address:

//  system("ifconfig eth0 192.168.0.2 netmask 255.255.255.0 up");
//  String setStaticIp = "ifconfig eth0 192.168.0.5 netmask 255.255.255.0 up";
  String setStaticIp = "ifconfig eth0 " + stationIp +" netmask 255.255.255.0 up";
  Serial.println(serverIp);
  Serial.println(stationIp);
  systemOut(setStaticIp);
}

void loop() {
  Serial.print(stationId);
  Serial.println(" is here");
  
//  system("ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}' > /dev/ttyGS0");

  String test = "curl --data \"braceletId=" + String( (int)random(200) ) + "&stationId=" + String(stationId) + "&points=" + String(10101100) + "\" " + serverIp + "\n";
  systemOut(test);
  
//  system("curl --data \"braceletId=101&stationId=202&points=11001100110011\" http://192.168.0.11:8080");  
  
  delay(5000);
}

void systemOut(String input){
  int len = input.length();
  char inputChar[len];
  input.toCharArray(inputChar, len);
  system(inputChar);  
}

int getId(char file[]){
  char output[3];
  FILE *fp;
  fp = fopen(file, "r");
  fgets(output, 3, fp);
  fclose(fp); 
  return atoi(output);
}
