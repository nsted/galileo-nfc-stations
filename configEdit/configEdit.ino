#include <SPI.h>
#include <SD.h>

#define STATION_ID (4)        // change this to match the number on the NFC scanner case (sd card)


#define STATION_IP ("172.16.19." + String(STATION_ID))
//#define SERVER ("192.168.1.50:8000")
#define SERVER ("172.16.19.246/api/core")
#define AUTHKEY ("abcdef")
#define GATEWAY ("10.2.0.1")
//#define GATEWAY ("172.16.19.1")
#define DNS_1 ("8.8.8.8")            
#define DNS_2 ("8.8.4.4")

String path = "/home/root/nfc-params/";
String output;

File myFile;

void setup()
{
 // Open serial communications and wait for port to open:
  Serial.begin(57600);
   while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  output = "mkdir " + path + "\n";
  systemOut(output);

  output = "touch " + path + "stationId.txt\n";
  systemOut(output);
  
  output = "touch " + path + "stationIp.txt\n";
  systemOut(output);

  output = "touch " + path + "serverIp.txt\n";
  systemOut(output);

  output = "touch " + path + "gateway.txt\n";
  systemOut(output);

  output = "touch " + path + "dns_1.txt\n";
  systemOut(output);  
  
  output = "touch " + path + "dns_2.txt\n";
  systemOut(output);  
  
//  system("touch /home/root/nfc-params/stationId.txt\n");
//  system("touch /home/root/nfc-params/stationIp.txt\n");
//  system("touch /home/root/nfc-params/serverIp.txt\n");  
//  system("touch /home/root/nfc-params/gateway.txt\n");
//  system("touch /home/root/nfc-params/dns_1.txt\n");
//  system("touch /home/root/nfc-params/dns_2.txt\n");
//  systemOut(output);

  output = "echo " + String(STATION_ID) + " > " + path + "stationId.txt\n";
  systemOut(output);
  
  output = "echo " + String(STATION_IP) + " > " + path + "stationIp.txt\n";
  systemOut(output);

  output = "echo " + String(SERVER) + " > " + path + "serverIp.txt\n";
  systemOut(output);

  output = "echo " + String(GATEWAY) + " > " + path + "gateway.txt\n";
  systemOut(output);

  output = "echo " + String(DNS_1) + " > " + path + "dns_1.txt\n";
  systemOut(output);  
  
  output = "echo " + String(DNS_2) + " > " + path + "dns_2.txt\n";
  systemOut(output);  

//  system("echo 18 > /home/root/nfc-params/stationId.txt\n");  
//  system("echo 192.168.1.18 > /home/root/nfc-params/stationIp.txt\n");  
//  system("echo 192.168.1.50:8000 > /home/root/nfc-params/serverIp.txt\n");    
//  system("echo 192.168.1.1 > /home/root/nfc-params/gateway.txt\n");      
//  system("echo 8.8.8.8 > /home/root/nfc-params/dns_1.txt\n");    
//  system("echo 8.8.4.4 > /home/root/nfc-params/dns_2.txt\n");  

//#define STATION_ID (18)

  Serial.print("Initializing SD card...");
  pinMode(10, OUTPUT);
   
  if (!SD.begin(4)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");
  
  // delete the old file
  SD.remove("config.txt");
  
  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  myFile = SD.open("config.txt", FILE_WRITE);
  
  // if the file opened okay, write to it:
  if (myFile) {
    Serial.print("Writing to config.txt...");
    
    myFile.print("STATION_ID ");
    myFile.println(STATION_ID);
    
//    myFile.print("MAC_ADDRESS ");
//    myFile.println(MAC_ADDRESS);
    
    // close the file:
    myFile.close();
    
    Serial.println("done.");
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening config.txt");
  }
  
  // re-open the file for reading:
  myFile = SD.open("config.txt");
  if (myFile) {
    Serial.println("config.txt:");
    
    // read from the file until there's nothing else in it:
    while (myFile.available()) {
    	Serial.write(myFile.read());
//        Serial.println();
    }
    // close the file:
    myFile.close();
  } else {
  	// if the file didn't open, print an error:
    Serial.println("error opening config.txt");
  }
}

void loop()
{
	// nothing happens after setup
}

void systemOut(String input){
  int len = input.length();
  char inputChar[len];
  input.toCharArray(inputChar, len);
  system(inputChar);  
}

