**NFCStation** runs NFC scanning stations that voluntarily track User's Booth Visits on a conference/trade show floor. Three types of stations are supported: 

1. Registration booths that store user data sent from a terminal on an NFC tag (a WristBand),
2. Coin booths that log user visits on the NFC Tag,
3. Prize booths that read User Data off NFC Tag, and send to a terminal.

Prototype Demo Here: http://youtu.be/sdfXmPGLM-8

####Hardware:
- Intel Galileo - http://arduino.cc/en/ArduinoCertified/IntelGalileo
- or Intel Edison with Arduino Breakout - https://www.adafruit.com/products/2180
- Adafruit NFC shield - https://learn.adafruit.com/adafruit-pn532-rfid-nfc
- or BM019 - http://www.solutions-cubed.com/bm019/
- Adafruit LCD I2C Backpack - https://learn.adafruit.com/i2c-spi-lcd-backpack
- 16x2 LCD - https://www.adafruit.com/product/181
- NFC Type 2 tag...eg. NTAG213. - more info at http://rapidnfc.com/which_nfc_chip
- or ISO1515693 tag
  
####Dependencies (place libraries in Arduino libraries folder).
- Modified Adafruit_NFCShield Library - https://bitbucket.org/nsted/adafruit_nfcshield_i2c
- Adafruit LiquidCrystal Library - https://github.com/adafruit/LiquidCrystal

####Using:
* Connect to the NFC station Serial Port.
* Registration**
    * Use a registration stations (station IDs 0 to 3) to load a user profile on a Tag. 
    * Transmit first name (up to 20 chars), and user ID (long int). Comma separate each field with no spaces. Terminate with <cr>.
    * Hold tag over scanner.
* **Coin Collection**
    * Use a coin station (station IDs 5 and up) to collect coins.
    * Hold tag in front of scanner.
* **Prize Redemption**
    * Use a prize station (station IDs 4) to claim prize.
    * Hold tag in front of scanner.

NB. All stations dump their user profile & coins after a transaction.

####Setup:
1. Open "configEdit.ino" 
2. Set the STATION_ID according to needs (multiple registration stations can share the same id). 
3. Customize the network IP addresses as needed
4. Upload the sketch.
5. Open "NFCstation.ino", and upload to the Station.

####Network:
Step 1 of **Setup** creates a number of files that contain the IP addresses relevant to the system, including:

* the NFC scanner's static IP.
* the Server IP
* the Gateway
* the DNS_1 IP
* the DNS_2 IP

Customize as needed

####TroubleShooting:
- If any erroneous info appears on LCD, hit reset button.
- A change in the code may require updating all the NFC stations.
-- in particular the coin stations cannot be made larger on the fly.
- On startup the station will print 4 IP addresses:
* ip: the expected address of the station (from stationIp.txt)
* sv: the expected address of the server (from stationIp.txt)
* gw: the expected address of the gateway (from gatewayIp.txt)
* addr: the actual address of the station (grepped from ifconfig)
--Make sure these match the addresses you need, and if not go back to **setup**
--Make sure that ip and addr match.
- Test ethernet with "networkTest/testServer_processing.pde", make sure to match the server address and port (eg. 127.0.0.1:8000)

####About:
Made by Nick Stedman, nick@moti.ph

####License: 
MIT